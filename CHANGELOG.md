//
// AcecoolLib - Changelog
//


//
// Tasks
//
	+ Port AcecoolLib
		Main Tasks

			A lot of things aren''t really necessary to port because of built-in solutions, or because of how different the systems are. Although I probably will and then see which one is faster... oh - port benchmarking system.

			+ Port x.This Helpers - draw.This, render.This, hook.This which used the cooldown / timekeeper systems to do something for x amount of time and then remove it..
			+ Port SQL Database Query Generation System and create connection system
			+ Port Timekeeper and cooldown system
			+ Port Data system with auto-save attributes, etc...
			+ Port Networking system - maybe... This system had automatic networking of tables when the data changed and instead of re-networking the entire table it only networked what changed...
			+ Port Team Management system
			+ Port Language system
			+ Port Libraries ( Math, lang, string, time, util, global - enum generator, etc.. )
			+ Port Benchmarking System
			+ Port Tree / Lined Printer... I''ll probably have to create an interface and extend everything with it for the output ... because it really isn''t the same as Lua just printing a giant table... Although for arrays, nested arrays, dictionaries, etc... it would be fine...
			+ Port
			+ Port
			+ Port
			+ Port
			+ Port
			+ Port Everything...


	+ Port / Create Behind Enemy Lines
	+ Port Crime City
	+ Create / Prototype Goodbye Sol ( Possibly Prototype it in this game )


	+ Create Action / Event system - generic hook system for the basic stuff using an ActionNode or EventNode for the args which can contain ... varargs.






//
// ChangeLog
//




-- 2021 - July 30

	+ Created AcecoolLib.Systems.Player.Clothing
		This system is injected / added to the Player via 1 line - this.Systems.Add( "Clothing", new AcecoolLib.Systems.Player.ClothingSystem( this ) );
		I am running into a few hurdles, but it does work. ( Just because it works, doesn't make it right. - Josh 'Acecool' Moser )... So if it isn't perfect, I may not use it. Any feedback?
		The other option is to do what I did when I originally set up the clothing system which is extend the partial AcecoolLib.Game.PlayerBase : Sandbox.Player class with functions such as AddClothing( ClothingType _id, string _mdl, string _body_part = "", int _body_index = 1 ), RemoveClothing( ClothingType _id ) and more
			but the system has those same functions. The difference is if you extend it, if I don't add an accessor system you'll basically have to use: AcecoolLib.Systems.Player.Clothing.Add( )... but you could always do using AcecoolLib.Systems.Player and then just do Clothing.Add... or AddClothing and using ..Clothing;

		So there is a bit to work on - but I do really like the systems idea because it automatically calls an initialize function, and a shutdown in appropriate areas, plus the ability to subscribe to events, etc.. A single line can be used to add or remove it from the game entirely so if you want to use it for your
			game-mode, a single line. If you want to make your own, a single line commented or removed. On top of this, I can set up a simple clothing system alternate which groups clothing types and then gives you a random outfit... instead of the main clothing system which will have outfits bound to teams / jobs...
			although I'll probably add more functionality to allow other features in the same system - but alternate systems for clothing


	+ Created AcecoolLib.IGameSystem
		This is an interface to define what a game system is and requires.
		Added Initialize, Shutdown and Tick as optional implementables.


	+ Created AcecoolLib.Container
		This is a base container using generics to contain data - note, right now it is really basic but I will adjust it in the future... Right now it is basically designed around a Dictionary Data var and implements those functions. Later, I will be porting my data
		storage system from Java or another from C++ which has a lot more features and is really fast at indexing, adding, removing, etc...


	+ Created AcecoolLib.GameSystems
		The container which stores each GameSystem - this system was created so that when someone uses AcecoolLib to create a game-mode, instead of having to edit the game object, which they can do if they want, they can just call Game Object Reference ( Current.AddSystem( id, gamesystem ) )
		which will invoke an event so other systems can get the reference to the system if they use it intimately, etc...

		The base game object also passes Tick, and other functions through each game system.


	+ Created AcecoolLib.TeamSystem
		Right now I am using a different team handler but I am converting that into a system. Right now this is empty.


	+ Created AcecoolLib.PlayerClothingSystem
		I will probably add a new namespace - AcecoolLib.Player and put it there... But this is also an empty system which will eventually be populated by the existing system I created which lets you add / remove clothing from a player easily. If the player is already wearing the type of clothing in question
		it is removed first before the new clothing is added - will likely make this configurable via console command and / or flat-file config var to define the behavior. Also an event is called which uses enumeration to define if clothing was added, removed, the placement enum, model string, and other info
		on the current system which is being ported to this game system.

		Note: This system will not be added to the base game object, but each player will have a systems var added to them too so when a player joins the server, a system can be added to that individual player. These systems will be very useful to add temporary, or permanent, functionality to the player
			game, or anything the systems object is added to. The idea is to make something extendable using addons / plugins without having to edit anything other than creating an event subscriber and adding the data.



-- 2021 - July
	+ Added



-- 2021 - June
	Started porting AcecoolLib into C#

	Started from scratch looking up the names, etc.. for calls to be properly routed.